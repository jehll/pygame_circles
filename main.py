import pygame
from random import randint

WIDTH, HEIGHT = 400, 300
RADIUS = 10
V = 100  # скорость

screen = pygame.display.set_mode((WIDTH, HEIGHT))


class Circle:
    def __init__(self, x, y, colour):
        self.x = x
        self.y = y
        self.color = colour
        self.clock = pygame.time.Clock()


circles = []

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.MOUSEBUTTONDOWN:
            circles.append(Circle(*event.pos, [randint(0, 255) for _ in range(3)]))

    screen.fill((0, 0, 0))
    for c in circles:
        if c.y + RADIUS < HEIGHT:
            pygame.draw.circle(screen, c.color, (c.x, int(c.y)), RADIUS)
            c.y += V * c.clock.tick() / 1000
        else:
            pygame.draw.circle(screen, c.color, (c.x, HEIGHT - RADIUS), RADIUS)
    pygame.display.flip()

pygame.quit()
